# Set the base image for subsequent instructions
FROM php:8.2-fpm-alpine

# Set working directory
WORKDIR /var/www

# # Install dependencies
RUN apk add php-session \
    php-tokenizer \
    php-xml \
    php-ctype \
    php-curl \
    php-dom \
    php-fileinfo \
    php-mbstring \
    php-openssl \
    php-pdo \
    php-pdo_mysql \
    php-session \
    php-tokenizer \
    php-xml \
    php-ctype \
    php-xmlwriter \
    php-simplexml \
    composer

RUN docker-php-ext-install mysqli pdo_mysql
RUN docker-php-ext-enable mysqli pdo_mysql
# # Clear cache
# RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install Composer
# COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# ENV PATH=~/.composer/vendor/bin:$PATH

CMD ["php", "artisan", "serve", "--host", "0.0.0.0", "--port", "8000"]
