build:
	docker-compose build
	docker network inspect unusida-netrwork >/dev/null 2>&1 || \
    docker network create --driver bridge unusida-netrwork
	make composer-install
	make npm-install

run-dev:
	docker-compose up -d

logs-app:
	docker logs my-laravel-app -f

logs-node:
	docker logs my-laravel-node -f

check-db:
	docker exec -it my-mysql mysql -u root -p -D unusida_workshop

composer-install:
	docker run --rm -it -v ./:/var/www my-laravel-app composer install

npm-install:
	docker run --rm -it -v ./:/var/www my-laravel-node npm install


run-first:
	docker exec -it my-laravel-app php artisan migrate 
	docker exec -it my-laravel-app php artisan db:seed
