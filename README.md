## Preparation
Untuk preparatin silahkan baca di file berikut:
- [Using Docker.md](https://gitlab.com/kangman.box/unusida-workshop/-/blob/main/Using%20Docker.md?ref_type=heads)
- [Without Docker.md](https://gitlab.com/kangman.box/unusida-workshop/-/blob/main/Without%20Docker.md?ref_type=heads)


## Routes Backend

### [GET] /books

- **Response**
```
{
    "books": [
        {
            "id": 1,
            "isbn": "978-623-5712-85-7",
            "title": "Adat Telaah Umum Pada Melayu Jambi",
            "category": "Ensiklopedi",
            "image_url": "https:\/\/www.salimmedia.com\/wp-content\/uploads\/2023\/02\/139af924-18d3-4eb1-8c49-5c8881aeb3da.jpg",
            "author": "Dr. Musa, S.Ag., M.Pd.I",
            "publisher": "CV. Salim Media Indonesia",
            "year": 2023,
            "price": 85000,
            "created_at": null,
            "updated_at": null
        }
    ]
}
```

### [POST] /books

- **Request**
```
{
    "isbn": "978-623-5712-85-7",
    "title": "Adat Telaah Umum Pada Melayu Jambi",
    "category": "Ensiklopedi",
    "image_url": "https:\/\/www.salimmedia.com\/wp-content\/uploads\/2023\/02\/139af924-18d3-4eb1-8c49-5c8881aeb3da.jpg",
    "author": "Dr. Musa, S.Ag., M.Pd.I",
    "publisher": "CV. Salim Media Indonesia",
    "year": 2023,
    "price": 85000,
}
```


### [DELETE] /books/{id}**

### Kategori buku
```['Antologi', 'Biografi', 'Cergam', 'Ensiklopedi', 'Karya Ilmiah', 'Komik', 'Majalah', 'Novel', 'Tafsir']```

### Data Header
```
[
    { title: 'Judul', value: 'title' },
    { title: 'ISBN', value: 'isbn' },
    { title: 'Gambar', value: 'image_url', sortable: false },
    { title: 'Kategori', value: 'category' },
    { title: 'Penulis', value: 'author' },
    { title: 'Penerbit', value: 'publisher' },
    { title: 'Tahun', value: 'year' },
    { title: 'Harga', value: 'price' },
    { title: 'Actions', key: 'actions', sortable: false },
]
```