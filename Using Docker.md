## Preparation (Linux)

## Requirement

- **Install [Docker (Linux)](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)**
- **Install [Git Versioning](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) (Untuk mendapatkan source code)**

### Untuk menjalankan Backend Laravel, jalankan perintah berikut secara berurutan di terminal:
- `git clone https://gitlab.com/kangman.box/unusida-workshop.git` -> pindah ke directory yang sudah di clone
- copy .env.example menjadi .env dengan perintah `cp .env.example .env`
- `make build`
- `make run-dev`
- `make run-first`

### Untuk mengetahui status container gunakan perintah berikut:
- `make logs-app` untuk status laravel
- `make logs-node` untuk status vite

### Untuk masuk ke mysql cli gunakan perintah berikut:
- `make check-db` masukkan password = "" (kosong)

### Setelah menjalankan perintah di atas, check apakah servis sudah berhasil berjalan dengan cara buka browser dengan alamat `http://localhost:8000`

### Mari kita gaskeun coding

## Preparation (Windows)

## Requirement

- **Install [Docker (Windows)](https://docs.docker.com/desktop/install/windows-install/)**
- **Install [Git Versioning](https://git-scm.com/download/win) (Untuk mendapatkan source code)**

### Untuk menjalankan Backend Laravel, jalankan perintah berikut secara berurutan di git bash:
- `git clone https://gitlab.com/kangman.box/unusida-workshop.git` -> pindah ke directory yang sudah di clone
- copy .env.example menjadi .env dengan perintah `cp .env.example .env`
- `docker compose build`
- `docker network create unusida-network`
- `docker run --rm -it -v ./:/var/www my-laravel-app composer install`
- `docker run --rm -it -v ./:/var/www my-laravel-node npm install`
- `docker compose up -d`
- `docker exec -it my-laravel-app php artisan migrate`
- `docker exec -it my-laravel-app php artisan db:seed`

### Untuk mengetahui status container gunakan perintah berikut:
- `docker logs my-laravel-app -f` untuk status laravel
- `docker logs my-laravel-node -f` untuk status vite

### Untuk masuk ke mysql cli gunakan perintah berikut:
- `docker exec -it my-mysql mysql -u root -p -D unusida_workshop` masukkan password = "" (kosong)

### Setelah menjalankan perintah di atas, check apakah servis sudah berhasil berjalan dengan cara buka browser dengan alamat `http://localhost:8000`

### Mari kita gaskeun coding

