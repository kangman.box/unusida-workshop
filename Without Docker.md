## Preparation (Windows)

## Requirement

- **Install [Git Versioning](https://git-scm.com/download/win) (Untuk mendapatkan source code)**
- **Install [XAMPP](https://www.apachefriends.org/download.html) (Untuk MySQL)**
- **Install [Composer](https://getcomposer.org/download/) (Untuk menginstall vendor laravel)**
- **Install [NodeJS](https://nodejs.org/en/download/package-manager) (Untuk menginstall package npm)**

### Untuk menjalankan Backend Laravel, jalankan perintah berikut secara berurutan di git bash:
- `git clone https://gitlab.com/kangman.box/unusida-workshop.git` -> pindah ke folder yang sudah di clone
- copy `.env.example` menjadi `.env`
- `composer install`
- `npm install`
- `php artisan migrate`
- `php artisan db:seed`
- `php artisan serve`
- Buka 1 window git bash di folder yang sama lalu jalankan perintah `npm run dev`

### Setelah menjalankan perintah di atas, check apakah servis sudah berhasil berjalan dengan cara buka browser dengan alamat `http://localhost:8000`

### Mari kita gaskeun coding