<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Books;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $books = Books::all();
        return response()->json(['books' => $books]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Books::updateOrCreate(["id" => $request->id], $request->except('id'));
        if ($request->id) {
            $message = 'Data buku berhasil diubah.';
        } else {
            $message = 'Data buku berhasil ditambahkan.';
        };

        return response()->json(['message' => $message]);
    }

    public function destroy($id)
    {
        Books::find($id)->delete();

        return response()->json(['message' => 'Data buku berhasil dihapus.']);
    }
}
