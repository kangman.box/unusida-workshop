<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Book
 * 
 * @property int $id
 * @property string $title
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @package App\Models
 */
class Books extends Model
{
	protected $table = 'books';


	protected $fillable = [
		'id', 'isbn', 'title', 'category', 'image_url', 'author', 'publisher', 'year', 'price'
	];

	public static $rules = [
		'isbn' => 'required',
		'title' => 'required',
		'author' => 'required',
		'price' => 'required',
	];
}
