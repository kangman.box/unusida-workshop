<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Books;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        User::factory()->create([
            'name' => 'Administrator',
            'email' => 'admin@unusida.ac.id',
        ]);

        DB::insert("INSERT INTO `books` (isbn, title, category, image_url, author, publisher, year, price) VALUES ('978-623-5712-85-7','Adat Telaah Umum Pada Melayu Jambi','Ensiklopedi','https://www.salimmedia.com/wp-content/uploads/2023/02/139af924-18d3-4eb1-8c49-5c8881aeb3da.jpg','Dr. Musa, S.Ag., M.Pd.I','CV. Salim Media Indonesia',2023,85000),('978-602-70308-4-8','Teknik dasar & peraturan permainan futsal','Karya Ilmiah','https://www.salimmedia.com/wp-content/uploads/2019/07/5.png','Adhe Saputra dan Hendri Munar','CV. Salim Media Indonesia',2015,70000),('978-623-7638-14-8','Aku Mencintaimu dengan Caraku','Novel','https://www.salimmedia.com/wp-content/uploads/2020/07/aku-mencintaimu-dengan-caraku-1.jpg','Ili Yanti','CV. Salim Media Indonesia',2020,65000);");

        
    }
}
